package pl.szymon.tictactoe;

import java.util.Scanner;

public class Player implements PlayerInterface {

    private String name;

    public static Player loadPlayer(String playerPosition) {
        System.out.println("Choose " + playerPosition + " player name");
        Scanner consoleScanner = new Scanner(System.in);
        String readLine = consoleScanner.nextLine();

        Player newPlayer = new Player();
        newPlayer.setName(readLine);
        return newPlayer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Player: " + name;
    }
}
