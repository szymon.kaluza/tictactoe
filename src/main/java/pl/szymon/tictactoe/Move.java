package pl.szymon.tictactoe;

import java.util.Scanner;

public class Move {
    private int fieldNumber;

    static Move loadMove(String name) {
        Move move = new Move();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Player's " + name + " move");

        System.out.println("Choose field");
        move.setFieldNumber(scanner.nextInt());

        return move;
    }


    public int getFieldNumber() {
        return fieldNumber;
    }

    public void setFieldNumber(int fieldNumber) {
        this.fieldNumber = fieldNumber;
    }


}


