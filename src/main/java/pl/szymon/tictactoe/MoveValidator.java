package pl.szymon.tictactoe;

public class MoveValidator {

    private int[] boardFields;

    public MoveValidator(int[] boardFields) {
        this.boardFields = boardFields;
    }

    public boolean isMoveCorrect(Move moveToCheck) {
        int numberOfFieldToCheck = moveToCheck.getFieldNumber() - 1;
        if (numberOfFieldToCheck >= 0 && numberOfFieldToCheck < 9) {
            return boardFields[numberOfFieldToCheck] == 0;

        }
        return false;
    }
}
