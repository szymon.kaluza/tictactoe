package pl.szymon.tictactoe;

public interface PlayerInterface {

    String getName();

    void setName(String name);

}
