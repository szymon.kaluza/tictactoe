package pl.szymon.tictactoe;

public class EndGameValidator {

    private int[] boardFields;

    public EndGameValidator(int[] boardFields) {
        this.boardFields = boardFields;
    }

    public int[][] oneDimensionalArrayIntoTwoDimension(int[] insert) {
        int sideLength = (int) Math.sqrt((insert.length));
        int counter = 0;
        int[][] twoD = new int[sideLength][sideLength];
        for (int row = 0; row < sideLength; row++) {
            for (int col = 0; col < sideLength; col++) {
                twoD[row][col] = insert[counter];
                counter++;
            }
        }
        return twoD;
    }

    static boolean testIfSomeoneWonOnMainDiagonal(int[][] insert) {

        for (int i = 1; i < insert.length; i++) {

            if (insert[i][i] == 0 || (insert[i][i] != insert[i - 1][i - 1])) {
                return false;
            }
        }

        return true;
    }

    static boolean testIfSomeoneWonOnMinorDiagonal(int[][] insert) {

        for (int i = 1; i < insert.length; i++) {

            if (insert[i][insert.length - 1 - i] == 0 || (insert[i][insert.length - 1 - i] != insert[i - 1][insert.length - i])) {
                return false;
            }
        }
        return true;
    }

    static boolean testIfSomeoneWonOnRow(int[][] insert) {
        for (int numberOfRow = 0; numberOfRow < insert.length; numberOfRow++) {
            if (checkEqualityForRows(insert, numberOfRow)) {
                return true;
            }
        }
        return false;
    }

    static boolean checkEqualityForRows(int[][] insert, int numberOfRow) {
        for (int numberOfColumn = 1; numberOfColumn < insert.length; numberOfColumn++) {
            if (insert[numberOfRow][0] == 0 || (insert[numberOfRow][0] != insert[numberOfRow][numberOfColumn])) {
                return false;
            }
        }
        return true;
    }

    static boolean testIfSomeoneWonOnColumn(int[][] insert) {
        for (int numberOfColumn = 0; numberOfColumn < insert.length; numberOfColumn++) {
            if (checkEqualityForColumns(insert, numberOfColumn)) {
                return true;
            }
        }
        return false;
    }

    static boolean checkEqualityForColumns(int[][] insert, int numberOfColumn) {
        for (int numberOfRow = 1; numberOfRow < insert.length; numberOfRow++) {
            if (insert[0][numberOfColumn] == 0 || (insert[0][numberOfColumn] != insert[numberOfRow][numberOfColumn])) {
                return false;
            }
        }
        return true;
    }


    private boolean isBoardFull() {
        for (int e : boardFields) {
            if (e == 0) {
                return false;
            }
        }
        return true;
    }


    public boolean isGameEnded() {
        int[][] twoD = oneDimensionalArrayIntoTwoDimension(boardFields);
        return testIfSomeoneWonOnMainDiagonal(twoD) ||
                (testIfSomeoneWonOnMinorDiagonal(twoD)) ||
                (testIfSomeoneWonOnRow(twoD)) ||
                (testIfSomeoneWonOnColumn(twoD)) ||
                (isBoardFull());
    }
}
