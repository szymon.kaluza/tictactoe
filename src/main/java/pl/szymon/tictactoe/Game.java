package pl.szymon.tictactoe;

public class Game {
    private Player circle;
    private Player cross;
    private Board board;
    private int giveNextPlayerFunctionCounter = 0;

    public Game(Board board){
        this.board = board;
    }

    void setPlayers(Player circle, Player cross) {
        this.circle = circle;
        this.cross = cross;
    }

    void showPlayers() {
        System.out.println("circle: " + circle);
        System.out.println("cross: " + cross);
    }


    public void setMove(Move moveToPerform, Player whoseMove) {
        int fieldOnTheBoard = moveToPerform.getFieldNumber() - 1;
        if (whoseMove == circle) {
            board.setField(fieldOnTheBoard, 1);
        } else {
            board.setField(fieldOnTheBoard, 2);
        }
    }


    public Player giveNextPlayer() {
        Player returnedPlayer;
        if (giveNextPlayerFunctionCounter % 2 == 0) {
            returnedPlayer = circle;
        } else {
            returnedPlayer = cross;
        }
        giveNextPlayerFunctionCounter += 1;

        return returnedPlayer;
    }


    public int getGiveNextPlayerFunctionCounter() {
        return giveNextPlayerFunctionCounter;
    }
}
