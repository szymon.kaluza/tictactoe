package pl.szymon.tictactoe;

public class Main {
    public static void main(String[] args) {
        Board board = new Board();
        Game game = new Game(board);
        MoveValidator moveValidator = new MoveValidator(board.getBoardFields());
        EndGameValidator endGameValidator = new EndGameValidator(board.getBoardFields());
        Player playerCircle = Player.loadPlayer("circle");
        Player playerCross = Player.loadPlayer("cross");

        game.setPlayers(playerCircle, playerCross);
        game.showPlayers();
        Player currentPlayer = null;
        while (!endGameValidator.isGameEnded()) {
            currentPlayer = game.giveNextPlayer();
            Board.drawBoard(board.getBoardFields());
            Move move = Move.loadMove(currentPlayer.getName());

            while (!moveValidator.isMoveCorrect(move)) {
                System.out.println("Choose correct field");
                Board.drawBoard(board.getBoardFields());
                move = Move.loadMove(currentPlayer.getName());
            }
            game.setMove(move, currentPlayer);
        }
        Board.drawBoard(board.getBoardFields());
        if (game.getGiveNextPlayerFunctionCounter() == 9) {
            System.out.println("Tie");
        } else {
            System.out.println("The winner is " + currentPlayer);
        }
    }
}
