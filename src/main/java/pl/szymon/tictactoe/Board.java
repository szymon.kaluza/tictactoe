package pl.szymon.tictactoe;

import java.util.Arrays;

public class Board {

    private int[] boardFields = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};

    static void drawBoard(int[] insert) {

        int[] insertTable = Arrays.copyOf(insert, insert.length);
        for (int i = 0; i < insertTable.length; i++) {
            int tableElement = insertTable[i];
            String printCharacter = convertNumberToChar(tableElement);
            System.out.print(printCharacter);
            if ((i + 1) % 3 == 0) {
                System.out.println();
            }
        }
        System.out.println();

    }

    public void setField(int numberOfField, int whichPlayer) {
        boardFields[numberOfField] = whichPlayer;
    }

    private static String convertNumberToChar(int tableElement) {
        String printCharacter;
        if (tableElement == 0) {
            printCharacter = "_";
        } else if (tableElement == 1) {
            printCharacter = "O";
        } else if (tableElement == 2) {
            printCharacter = "X";
        } else {
            printCharacter = "";
        }
        return printCharacter;
    }

    public static int convertCharToNumber(char input) {
        int returnValue;
        if (input == '_') {
            returnValue = 0;
        } else if (input == 'o') {
            returnValue = 1;
        } else if (input == 'x') {
            returnValue = 2;
        } else {
            throw new IllegalArgumentException("Supported characters: _ , 0, X");
        }
        return returnValue;
    }


    public int[] getBoardFields() {
        return boardFields;
    }

}
