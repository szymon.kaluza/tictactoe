package pl.szymon.tictactoe;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class EndGameValidatorTest {

    private EndGameValidator objectUnderTest;
    private int[] testingFields = new int[9];

    @Before
    public void before() {
        for (int i = 0; i < testingFields.length; ++i) {
            testingFields[i] = 0;
        }
        objectUnderTest = new EndGameValidator(testingFields);
    }
    @Test
    public void whyValidatorIsBad(){
        //given
        String s = "OXO\n" +
                "XOX\n" +
                "O__";
        //when
        writeBoard(s, testingFields);
        boolean result = objectUnderTest.isGameEnded();

        //then
        assertThat(result).isTrue();
    }

    private void writeBoard(String input, int[] testingFields) {
        String replace = input.replaceAll("\n", "");
        if (replace.length() != 9) {
            throw new IllegalArgumentException("Supported board is 3x3");
        }
        char[] chars = replace.toLowerCase().toCharArray();
        for (int i = 0; i < chars.length; i++) {
            testingFields[i] = Board.convertCharToNumber(chars[i]);

        }
    }
}