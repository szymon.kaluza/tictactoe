package pl.szymon.tictactoe;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PlayerTest {

    private Player objectUnderTest;

    @Before
    public void before() {
        objectUnderTest = new Player();
    }

    @Test
    public void shouldToStringContainName() {
        // given
        final String testingName = "Szymon";
        objectUnderTest.setName(testingName);

        // when
        String resultToString = objectUnderTest.toString();

        // then
        boolean isContainName = resultToString.contains(testingName);
        assertTrue(isContainName);
    }
}