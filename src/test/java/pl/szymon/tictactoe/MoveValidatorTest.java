package pl.szymon.tictactoe;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MoveValidatorTest {

    private MoveValidator objectUnderTest;
    private int[] testingFields = new int[9];

    @Before
    public void before() {
        for (int i = 0; i < testingFields.length; ++i) {
            testingFields[i] = 0;
        }
        objectUnderTest = new MoveValidator(testingFields);
    }


    @Test
    public void shouldMoveBeNegative() {
        //given
        final Move testMove = new Move();
        testMove.setFieldNumber(-7);

        //when
        boolean result = objectUnderTest.isMoveCorrect(testMove);

        //then
        assertFalse(result);
    }

    @Test
    public void shouldAllowForCorrectMove() {
        //given
        final Move testMove = new Move();
        testMove.setFieldNumber(5);

        //when
        boolean result = objectUnderTest.isMoveCorrect(testMove);

        //then
        assertTrue(result);
    }

    @Test
    public void shouldRejectMoveFromBeyondBoard() {
        //given
        final Move testMove = new Move();
        testMove.setFieldNumber(testingFields.length + 1);

        //when
        boolean result = objectUnderTest.isMoveCorrect(testMove);

        //then
        assertFalse(result);
    }

    @Test
    public void shouldRejectMoveOnTakenField() {
        //given
        final Move testMove = new Move();
        testMove.setFieldNumber(0);
        testingFields[0] = 1;

        //when
        boolean result = objectUnderTest.isMoveCorrect(testMove);

        //then
        assertFalse(result);

    }

}